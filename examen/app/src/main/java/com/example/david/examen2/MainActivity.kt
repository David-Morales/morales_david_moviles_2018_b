package com.example.david.examen2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.util.Log

class MainActivity : AppCompatActivity() {

    internal lateinit var randomTimeTextView : TextView
    internal lateinit var pointsView: TextView
    internal lateinit var startButton: Button
    internal lateinit var pointsButton: Button

    internal lateinit var countDownTimer: CountDownTimer
    internal val countDownIntervval = 1000L
    internal val initialCountDown= 10000L

    var score = 0
    var randomTime=0
    var timeLeftAux=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        randomTimeTextView = findViewById<TextView>(R.id.guess_time)

        pointsView = findViewById<TextView>(R.id.points_view)
        pointsView.text= getString(R.string.points_view, Integer.toString(score))

        startButton = findViewById<Button>(R.id.start_button)
        startButton.setOnClickListener{_-> start()}

        pointsButton= findViewById<Button>(R.id.points_button)
        pointsButton.setOnClickListener{_-> compareTime()}

    }

    private fun generateRandomNumber(): Int {
        val n=(0..10).shuffled().first()
        return n
    }

    private fun start(){
        randomTime = generateRandomNumber()
        randomTimeTextView.text = getString(R.string.guess_time, Integer.toString(randomTime))
        generateCountDownTimer()
    }

    private fun generateCountDownTimer(){
        var timeLeft=0
        countDownTimer= object : CountDownTimer(initialCountDown,countDownIntervval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft= millisUntilFinished.toInt()/1000
                timeLeftAux=timeLeft
                Log.d("TAG","$timeLeftAux")
            }

            override fun onFinish() {

            }
        }
        countDownTimer.start()
    }

    private fun compareTime(){
        if(randomTime===timeLeftAux){
            score+=100
            pointsView.text= getString(R.string.points_view, Integer.toString(score))
        }else if(randomTime===timeLeftAux-1 || randomTime===timeLeftAux+1){
            score+=50
            pointsView.text= getString(R.string.points_view, Integer.toString(score))
        }
    }




}

